import React from 'react';
import MovieList from './MovieList';
import SearchBox from './SearchBox';
import { FormattedMessage } from 'react-intl';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import deepOrange from '@material-ui/core/colors/deepOrange';

const styles = theme => ({
  container: {
    flexGrow: 1
  },
  title: {
    flex: 1
  },
  orangeAvatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: deepOrange[500]
  }
});

export class Movies extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const name = 'Darrell';
    const { classes } = this.props;
    const nameAvatar = name.slice(0, 1);

    return (
      <div className={classes.container}>
        <div>
          <AppBar position="static" color="default">
            <Toolbar>
              <Avatar className={classes.orangeAvatar}>{nameAvatar}</Avatar>
              <Typography variant="title" className={classes.title}>
                <FormattedMessage
                  id="hello"
                  defaultMessage="Movies for {name}!!"
                  values={{ name: name }}
                />
              </Typography>
              <SearchBox />
            </Toolbar>
          </AppBar>
        </div>
        <MovieList />
      </div>
    );
  }
}

Movies.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Movies);
