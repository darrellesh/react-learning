import {
  changeMovieDB,
  START_SEARCH,
  CHANGE_SEARCH_PAGE,
  FETCH_MOVIE_PAGE
} from './actions';
import {
  takeEvery,
  takeLatest,
  call,
  put,
  select,
  all
} from 'redux-saga/effects';

import 'whatwg-fetch';
import {
  makeSelectSearchString,
  makeSelectSearchPageString
} from './selectors';

function parseJSON(response) {
  return response.json();
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

async function getData(url) {
  const result = await fetch(url, { method: 'GET' });

  try {
    checkStatus(result);
    return result.json();
  } catch (e) {
    throw e;
  }
}

function* search() {
  const apiKey = '057dfa32a18eed0f2dc23dc2e80ed8a0';
  const searchString = yield select(makeSelectSearchString());
  let searchPage = yield select(makeSelectSearchPageString());
  searchPage = searchPage + 1; //zero based TablePagination
  const baseUrl = 'https://api.themoviedb.org/3/search/movie?';
  const url =
    baseUrl +
    'page=' +
    searchPage +
    '&&include_adult=false&language=en-US&api_key=' +
    apiKey +
    '&query=' +
    searchString;

  try {
    const data = yield call(getData, url);
    yield put(changeMovieDB(data));
  } catch (ex) {}
}

export function* watchForSearchActions() {
  yield takeEvery(START_SEARCH, search);
}

export function* watchForSearchPageActions() {
  yield takeLatest(FETCH_MOVIE_PAGE, search);
}

export default function* rootSaga() {
  yield all([watchForSearchActions(), watchForSearchPageActions()]);
}
