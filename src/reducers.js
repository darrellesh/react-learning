import { combineReducers } from 'redux-immutable';
import { fromJS, Map } from 'immutable';
import {
  CHANGE_MOVIEDB,
  CHANGE_SEARCH_STRING,
  CHANGE_SEARCH_PAGE
} from './actions';
import { reduce } from 'lodash';

const initialState = fromJS({
  searchString: '',
  moviesDB: {},
  moviesPagable: { totalPages: 0, totalResults: 0, page: 0 }
});

const buildMovieMap = movies =>
  reduce(
    movies,
    (result, movie) => result.set(movie.id, fromJS(movie)),
    new Map()
  );

function movies(state = initialState, action) {
  switch (action.type) {
    case CHANGE_SEARCH_STRING:
      return state.set('searchString', action.searchString);
    case CHANGE_MOVIEDB:
      return state
        .set('moviesDB', buildMovieMap(action.movies.results))
        .setIn(['moviesPagable', 'totalPages'], action.movies.total_pages)
        .setIn(['moviesPagable', 'totalResults'], action.movies.total_results)
        .setIn(['moviesPagable', 'page'], action.movies.page);
    case CHANGE_SEARCH_PAGE:
      return state.setIn(['moviesPagable', 'page'], action.searchPage);
    default:
      return state;
  }
}

export const moviesApp = combineReducers({
  movies
});
