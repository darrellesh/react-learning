import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { withStyles } from '@material-ui/core';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import { makeSelectMovies, makeSelectMoviesPagable } from './selectors';
import * as actions from './actions';

import Paper from '@material-ui/core/Paper';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: 'lightBlue',
    color: theme.palette.common.black
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const styles = theme => ({
  root: {
    marginTop: '20px',
    width: '100%'
  },
  table: {
    minWidth: 700
  },
  overview: {
    width: '50%'
  }
});

export class MovieList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { rowsPerPage: 20, currentPage: 0 };
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleChangePage = (event, currentPage) => {
    this.setState({ currentPage });
    this.handleChangeSearchPage(currentPage);
    this.handleFetchMoviePage();
  };

  handleChangeSearchPage = currentPage => {
    this.props.actions.changeSearchPage(currentPage);
  };

  handleFetchMoviePage = () => {
    this.props.actions.fetchMoviePage();
  };

  render() {
    const {
      classes,
      movies,
      moviesPagable: { totalPages, totalResults }
    } = this.props;
    const { rowsPerPage, currentPage } = this.state;
    return (
      <Paper className={classes.root} elevation={5}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <CustomTableCell>Title</CustomTableCell>
              <CustomTableCell>Release Date</CustomTableCell>
              <CustomTableCell>Summary</CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {movies.map(n => {
              return (
                <TableRow key={n.id}>
                  <TableCell>{n.title}</TableCell>
                  <TableCell>{n.release_date}</TableCell>
                  <TableCell className={classes.overview}>
                    {n.overview}
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[20]}
                colSpan={3}
                count={totalResults}
                rowsPerPage={rowsPerPage}
                page={currentPage}
                SelectProps={{
                  native: true
                }}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
                onChangePage={this.handleChangePage}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </Paper>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

MovieList.propTypes = {
  classes: PropTypes.object.isRequired,
  movies: PropTypes.array
};

const mapStateToProps = createStructuredSelector({
  movies: makeSelectMovies(),
  moviesPagable: makeSelectMoviesPagable()
});

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(MovieList)
);
